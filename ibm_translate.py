import ezodf
from ibm_watson import LanguageTranslatorV3
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator

def translate(list_of_words):
    """
    Contacts the IBM Watson API, provides it with an API key and request for
    argument list_of_words (provided by getwords() -> main())to be translated.
    Returns list of translations after sanitizing original JSON.
    """
    authenticator = IAMAuthenticator('api-key')
    language_translator = LanguageTranslatorV3(
        version='2018-05-01',
        authenticator=authenticator)

    language_translator.set_service_url('url')

    translation = language_translator.translate(
        text=list_of_words,
        model_id='ms-en').get_result()

    list_of_translations = []
    for translation_dict in translation["translations"]:
        list_of_translations.append(translation_dict["translation"])

    return list_of_translations

def get_words(file_path, file_type):
    """
    Opens file according to argument file_path and file_type,
    saves content in file to list_of_words and return it
    """
    list_of_words = []
    if file_type == 'txt':
        with open(file_path, "r") as text_file:
            for line in text_file:
                list_of_words.append(line.rstrip())
    else:
        sheet = ezodf.opendoc(file_path).sheets[0]
        for cell in sheet.column(0):
            list_of_words.append(cell.value.rstrip())

    return list_of_words

def main():
    print("\nWelcome to the translator!"
          " Please enter the path of the file that you desire to translate: ")
    file_path = input()

    print("Processing...")

    # detect if file is .txt/.ods
    file_type = ''
    for character in file_path[::-1]:
        if character == ".":
            break
        file_type += character
    if file_type[::-1] not in ['ods', 'txt']:
        print("Invalid file type.")
        main()

    list_of_words = get_words(file_path, file_type)
    list_of_translations = translate(list_of_words)

    #write original word and translation to file
    if file_type == 'txt':
        with open(file_path, "r+") as text_file:
            for i in enumerate(list_of_words):
                text_file.write("{0}     {1} \n".format(list_of_words[i], list_of_translations[i]))
    else:
        sheet = ezodf.opendoc(file_path).sheets[0]
        # sheet.append_columns(1)
        # insert a single word from the list into a single cell in column B
        for index in range(len(list_of_translations)):
            sheet[index, 1].set_value(list_of_translations[index])
            pass

    print("Done.\n")

if __name__ == "__main__":
    main()
