# translator

A python program that takes a text file with words separated by a newline and feeds it to the IBM Watson Language Translation service.
Requires a free IBM cloud account.

**Note**: This project is still incomplete and still lack many features and support for various file types.
At this stage, the program works only on `.txt` ~~and `.ods`~~ files, and is spaghetti code.

## Usage
```
$ python3 ibm_translate.py
```
The program will read and write words and translations to the file path given.

## Example
```
$ python3 ibm_translate.py 

Welcome to the translate program! Please enter your the path of the file that you desire to translate: 
/home/user/Documents/example.txt
Processing...
Done.
```

